import React from 'react';
import '../styles/Reg.css';
import axios from "axios";

const Reg = () => {
    function handleClickSignUp(){
        let login = document.querySelector("#login");
        let password = document.querySelector("#password");
        let confirmpassword = document.querySelector("#confirmpassword");
        let email = document.querySelector("#email");
        let formData = {
            login: login.value,
            password: password.value,
            confirmpassword: confirmpassword.value,
            email: email.value
        }
        axios.post('/user', {
            'login': formData.login,
            'password': formData.password,
            'confirmpassword': formData.confirmpassword,
            'email': formData.email,
            'token': {},
        });
    }
    return (
        <div id="all">
            <header>
                <div id="header">Food Adviser</div>
            </header>
            <div className='Reg'>
                <div className="container">
                    <form action="/user" method="POST">
                        <div className='inputs'>
                            <div className='input' id="login"><input
                                name='login'
                                type='text'
                                placeholder='Login'/>
                            </div>
                            <div className='input' id="password"><input
                                name='password'
                                type='password' placeholder='Password'/>
                            </div>
                            <div className='input' id="confirmpassword"><input
                                name='confirmpassword'
                                type='text'
                                placeholder='Confirm password'/>
                            </div>
                            <div className='input' id="email"><input
                                name='email'
                                type='email' placeholder='Email'/>
                            </div>

                        </div>
                        <div className='buttons'>
                            <div className='button'>
                                <button onClick={handleClickSignUp} type='submit'>Sign Up</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Reg;