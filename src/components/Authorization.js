import React from 'react';
import '../styles/Auth.css';
import {useNavigate} from "react-router";
import axios from "axios";


const Auth = () => {
    function handleClickLogIn() {
        let login = document.querySelector("#log");
        let password = document.querySelector("#pas");
        let formData = {
            login: login.value,
            password: password.value
        }
        axios.post('/user', {
            'login': formData.login,
            'password': formData.password,
            'token': {},
        });
    }

    let navigate = useNavigate();

    function handleClickSignUp() {
        navigate("/reg");
    }

    return (
        <div id="all">
            <header>
                <div id="header">Food Adviser</div>
            </header>
            <div className='Auth'>
                <div className="container">
                    <form className="form">
                        <div className='inputs'>
                            <div className='input' id="login"><input id="log"
                                                                     name='login'
                                                                     type='text'
                                                                     placeholder='Login'/>
                            </div>
                            <div className='input'><input id="pas"
                                                          name='password'
                                                          type='password' placeholder='Password'/>
                            </div>
                        </div>
                    </form>
                    <div className='buttons'>

                        <div className='button'>
                            <button form='form' onClick={handleClickLogIn} type='submit'>Login</button>
                        </div>

                        <div className='button'>
                            <button onClick={handleClickSignUp}>Sign Up</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Auth;