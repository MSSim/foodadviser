import React from 'react';
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";
import Auth from "./Authorization";
import Reg from "./Registration";

class App extends React.Component {
    render() {
        return (
            <BrowserRouter >
                <Routes>
                    <Route path="/" element={<Auth/>} >
                    </Route>
                    <Route path="/reg" element={<Reg/>}>
                    </Route>
                </Routes>
            </BrowserRouter>
        );
    }
}

export default App;